﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LamHuuVinh
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.WriteLine("1. Tìm độ dài của chuỗi được nhập");
            Console.WriteLine("2. Đếm số từ trong chuỗi");
            Console.WriteLine("3. Đếm số nguyên âm và phụ âm trong chuỗi");
            Console.WriteLine("4. Tính tổng các chữ số của một số nguyên");
            Console.WriteLine("5. Sắp xếp mảng theo thứ tự giảm dần và in ra màn hình");
            Console.WriteLine("6. Nhập 1 số, tính giai thừa của số đó và xuất ra màn hình");
            Console.WriteLine("7. Nhập và xuất ra màn hình thông tin căn cước của 1 công dân Việt Nam");
            Console.WriteLine("8. Nhập số nguyên dương n và tìm n số Fibonacci");
            Console.Write("Mời nhập lựa chọn: ");
            int choose = int.Parse(Console.ReadLine());
            switch (choose)
            {
                case 1:
                    Bai1();
                    break;
                case 2:
                    Bai2();
                    break;
                case 3:
                    Bai3();
                    break;
                case 4:
                    Bai4();
                    break;
                case 5:
                    Bai5();
                    break;
                case 6:
                    Bai6();
                    break;
                case 7:
                    Bai7();
                    break;
                case 8:
                    Bai8();
                    break;

                default: return;
            }

        }

        static void Bai1()
        {
            Console.Write("Mời nhập chuỗi: ");
            string str = Console.ReadLine();
            int i = 0;
            foreach (char c in str)
            {
                i++;
            }
            Console.WriteLine("Độ dài của chuỗi là: {0}", i);

            Console.ReadKey();
        }
        static void Bai2()
        {
            Console.Write("Mời nhập chuỗi: ");
            string str = Console.ReadLine();
            string[] words = str.Split(' ');
            int numberOfWords = words.Length;
            Console.WriteLine("Số từ trong chuỗi là: " + numberOfWords);

            Console.ReadKey();
        }
        static void Bai3()
        {
            Console.Write("Mời nhập chuỗi: ");
            string str = Console.ReadLine();
            int vowels = 0, consonants = 0;

            for (int i = 0; i < str.Length; i++)
            {
                char ch = Char.ToLower(str[i]);
                if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u')
                {
                    vowels++;
                }
                else if (ch >= 'a' && ch <= 'z')
                {
                    consonants++;
                }
            }

            Console.WriteLine("Số chữ cái nguyên âm: " + vowels);
            Console.WriteLine("Số chữ cái phụ âm: " + consonants);

            Console.ReadKey();
        }
        static void Bai4()
        {
            Console.Write("Mời nhập số nguyên: ");
            int number = int.Parse(Console.ReadLine());
            int sum = 0;

            while (number > 0)
            {
                int digit = number % 10;
                sum += digit;
                number /= 10;
            }
            Console.WriteLine("Tổng các chữ số của số nguyên là: " + sum);

            Console.ReadKey();
        }
        static void Bai5()
        {
            Console.Write("Nhập số ptử của mảng: ");
            int n = int.Parse(Console.ReadLine());
            int[] a = new int[n];
            Input(a, n);
            Arrange(a);

            Console.ReadKey();
        }

        private static void Input(int[] a, int n)
        {
            for (int i = 0; i < n; i++)
            {
                Console.Write("a[" + i + "]=");
                a[i] = int.Parse(Console.ReadLine());
            }
        }
        private static void Arrange(int[] a)
        {
            // Sắp xếp mảng theo thứ tự giảm dần
            Array.Sort(a);
            Array.Reverse(a);

            // In mảng ra màn hình
            foreach (int num in a)
            {
                Console.Write(num + " ");
            }

        }


        static void Bai6()
        {

        }
        static void Bai7()
        {

        }
        static void Bai8()
        {

        }
    }
      
}
